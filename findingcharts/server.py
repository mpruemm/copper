from urllib.parse import urlparse, parse_qsl
from urllib.request import urlopen, Request
from urllib.error import HTTPError
from http import HTTPStatus
import json
import uuid
import re
import io
import makefc

__version__ = '2018.01'

server_version = 'FCMaker/' + __version__


class Record:
    pass


def validate_params(params):
    args = Record()
    for k in ('name', 'ra', 'dec', 'fov', 'program'):
        setattr(args, k, params.get(k, ""))
    args.ra = makefc.right_ascension(args.ra)
    args.dec = makefc.declination(args.dec)
    args.fov = float(args.fov) if args.fov else 5.0
    return args


def makechart(params):
    args = validate_params(params)
    img = io.BytesIO()
    with makefc.search_dss(args.ra, args.dec, args.fov) as fitsdata:
        center = "%s %s" % (args.ra, args.dec)
        makefc.chart(img, fitsdata, args.name, center, args.program)
    return img


class P2API:
    def __init__(self, ob_url, access_token):
        url = urlparse(ob_url)
        assert re.fullmatch(r'.*/obsBlocks/\d+', url.path)
        self.url = ob_url
        self.auth = 'Bearer ' + access_token

    def _headers(self):
        return {'User-Agent': server_version,
                'Authorization': self.auth,
                'Accept': 'application/json'}

    def _send_request(self, request):
        try:
            with urlopen(request) as res:
                result = res.getcode(), res.info(), res.read()
        except HTTPError as e:
            result = e.code, e.hdrs, e.fp.read()
        return result

    def get_ob(self):
        h = self._headers()
        request = Request(self.url, headers=h)
        status, headers, body = self._send_request(request)
        if status == HTTPStatus.OK:
            body = json.loads(body.decode('utf8'))
        return status, headers, body

    def attach_fc(self, image, filename):
        h = self._headers()
        h['Content-Type'] = 'image/jpeg'
        h['Content-Disposition'] = 'inline; filename="%s"' % filename
        url = self.url +'/findingCharts'
        request = Request(url, data=image, headers=h, method='POST')
        return self._send_request(request)


class Responder:
    status_line = {v: '%d %s' % (v, v.phrase)
                   for v in HTTPStatus.__members__.values()}

    def __init__(self, env, start_response):
        self.env = env
        self.start_response = start_response

    def send(self, status, body, content_type):
        self.start_response((self.status_line.get(status) or
                            ('%d Unknown status' % status)),
                            [('Access-Control-Allow-Origin', '*'),
                             ('Access-Control-Expose-Headers', '*'),
                             ('Content-Type', content_type or 'text/plain')])
        return [body]

    def bad_request(self, message):
        self.start_response(self.status_line[HTTPStatus.BAD_REQUEST],
                            [('Access-Control-Allow-Origin', '*'),
                             ('Access-Control-Expose-Headers', '*'),
                             ('Content-Type', 'text/plain')])
        return [bytes(message, 'utf8')]


def get_chart(qs, resp):
    """Make finding chart and send it back in the response."""
    params = dict(parse_qsl(qs))
    try:
        jpegdata = makechart(params)
    except ValueError as e:
        return resp.bad_request(str(e))
    else:
        resp.start_response('200 OK',
                            [('Access-Control-Allow-Origin', '*'),
                             ('Access-Control-Expose-Headers', '*'),
                             ('Content-Type', 'image/jpeg')])
        return jpegdata


def attach_chart(params, resp):
    """Make finding chart and attach it to an OB."""

    # params required: url, access_token;
    # optional: ra, dec, name, fov, program, filename
    for k in ('url', 'access_token'):
        if k not in params:
            return resp.bad_request('property "%s" missing' % k)

    p2 = P2API(params['url'], params['access_token'])

    if 'ra' not in params or 'dec' not in params:
        status, headers, body = p2.get_ob()
        if status != HTTPStatus.OK:
            return resp.send(status, body, headers.get('Content-Type'))

        params.setdefault('name', body['name'])
        for k in ('ra', 'dec'):
            params[k] = body['target'][k]

    try:
        jpegdata = makechart(params)
    except ValueError as e:
        return resp.bad_request(str(e))

    # upload fc to p2
    filename = params.get('filename') or (str(uuid.uuid4()) + '.jpg')
    status, headers, body = p2.attach_fc(jpegdata.getvalue(), filename)
    if status == HTTPStatus.UNAUTHORIZED:
        return resp.bad_request('invalid access_token')
    return resp.send(status, body, headers.get('Content-Type'))


def fcmaker(env, start_response):
    responder = Responder(env, start_response)

    method = env['REQUEST_METHOD']
    if method == 'GET':
        qs = env.get('QUERY_STRING', '')
        if not qs:
            return responder.bad_request('parameters RA and DEC missing')

        return get_chart(qs, responder)

    elif method == 'POST':
        ct = env.get('CONTENT_TYPE', '').split(';')[0]
        if ct == 'application/json':
            params = json.load(env['wsgi.input'])
        elif ct == 'application/x-www-form-urlencoded':
            body = env['wsgi.input'].read()
            params = dict(parse_qsl(body.decode('utf8')))
        else:
            return responder.bad_request('bad "Content-Type"')

        return attach_chart(params, responder)

    elif method == 'OPTIONS':
        start_response('200 OK', [
            ('Content-Type', 'text/plain'),
            ('Access-Control-Allow-Origin', '*'),
            ('Access-Control-Allow-Methods', 'GET, POST, OPTIONS'),
            ('Access-Control-Allow-Headers', '*'),
            ('Access-Control-Max-Age', '300'),
            ])
        return [b'']

    else:
        start_response('405 Method not allowed', [('Content-Type', 'text/plain')])
        return [b'Method not allowed']


if __name__ == '__main__':
    import waitress
    waitress.serve(fcmaker, port=9988,
                   ident=server_version,
                   outbuf_overflow=1300000,
                   max_request_header_size=32768,
                   max_request_body_size=32768)

# vim: set ai et ts=4 sw=4:
