#!/usr/bin/env python3
# PACKAGE D S S - H R Z
# standalone finder generator
# syntax	
#   makeDSSfinder.py name hh:mm:ss,+dd:mm:ss  [ProgramId] [FoV (arcmin)]
#
# output
#   <name>.ps and <name>.jpg, the finders.
#
# author	O.Hainaut
# version
Oversion = "DSSfinder 2014-03-06 ohainaut"
#.history
#  2014-08-26 standalone DSS without ephem
#  2013-09-05 creation
#
#

import aplpy
import sys
import os


#Hardcoded default values:
FoV = 10.
Oprogram = 'Hainaut'

i = len(sys.argv)

print()
print(i)
if i == 1:
    print('Usage: makeDSSfinder.py name hh:mm:ss,+dd:mm:ss  [ProgramId] [fov] ')
    print('   ProgramId: optional program ID for finder')
    sys.exit(0)
else:
    Oname = sys.argv[1]
    OCoord = sys.argv[2]
    w = OCoord.split(',')
    print(w)
    ORA = w[0]
    ODec = w[1]
    if i >= 4:
        Oprogram = sys.argv[3]
        if i >= 5:
            FoV = sys.argv[4]



# produce the strings we'll need for the finder
OfileRoot = Oname
Oobject = Oname
Ocenter = ORA+ "  " +ODec

print(Ocenter)


# SEARCH THE DSS

# assemble web command line and URL

dssFile = 'D' + OfileRoot + '.fits'
dssHTML1 =  'wget -O ' + dssFile + '  '

dssHTML = 'http://archive.eso.org/dss/dss/image?mime-type=download-fits' 
dssHTML = dssHTML + '&ra=' + ORA + '&dec=' + ODec
dssHTML = dssHTML +  '&x=' + str(FoV) + '&y=' + str(FoV)


# search for a DSS with that field

for dssMode in ['&Sky-Survey=DSS2-red', '&Sky-Survey=DSS2-blue', '&Sky-Survey=DSS1']:
    # call the archive
    print("Calling Archive: ", dssHTML1+'"'+dssHTML+dssMode+'"')
    os.system(dssHTML1+'"'+dssHTML+dssMode+'"')

    # check if it was returned
    test = os.system('grep ERROR ' + dssFile)
    if test > 0 :
        break   # found a DSS!
    
else:
    print('ERROR- no DSS found')
    sys.exit(1)


# PREPARE THE FINDER

# load the DSS

fig = aplpy.FITSFigure(dssFile)
fig.show_grayscale(invert='true',pmin=0.25,pmax=99.75)
fig.tick_labels.set_font(size='small')



# labels
#fig.add_scalebar(0.0833333333333)
#fig.scalebar.set_label('5\'')
fig.add_scalebar(0.1133333)
fig.scalebar.set_label('6.8\' (FORS)')



fig.add_label(0.2, 1.1, 'Object:', relative=True, weight='bold', 
              size='x-large', horizontalalignment='right')
fig.add_label(0.2, 1.02, 'Field Center:', relative=True, weight='bold', 
              size='small', horizontalalignment='right')
fig.add_label(0.22, 1.1,  Oobject,   relative=True, weight='normal', 
              size='x-large', horizontalalignment='left')
fig.add_label(0.22, 1.02, Ocenter, relative=True, weight='normal', 
              size='small', horizontalalignment='left')
fig.add_label(1., -0.1,  Oprogram,   relative=True, weight='bold', 
              size='x-large', horizontalalignment='right')
fig.add_label(0.0, -0.1,  Oversion,   relative=True, weight='normal', 
              size='xx-small', horizontalalignment='left', color='grey')



# output

fig.save(OfileRoot+'.ps')
fig.save(OfileRoot+'.jpg')

sys.exit(0)
# -o-
