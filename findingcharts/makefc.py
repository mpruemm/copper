#!/usr/bin/env python3
# standalone finding chart generator
#
# author	O. Hainaut, M. Pruemm

import warnings
import argparse
from re import fullmatch
from urllib.request import urlopen, HTTPError
from urllib.parse import urlencode
from contextlib import closing

# suppress a warning generated when importing aplpy
import matplotlib
warnings.simplefilter("ignore", matplotlib.mplDeprecation)
matplotlib.use('Agg')

import astropy
import aplpy

# silence astropy
astropy.log.setLevel('ERROR')

# Oversion = "DSSfinder 2014-03-06 ohainaut"

__version__ = "2018.01"

program_version = "makefc " + __version__

DSS_URL = 'http://archive.eso.org/dss/dss/image'


# search for a DSS with that field

def search_dss(ra, dec, fov):
    """Search for a DSS image with that field."""
    fov = max(fov, 5.0)
    for mode in ("DSS2-red", "DSS2-blue", "DSS1"):
        qs = urlencode([("ra", ra),
                        ("dec", dec),
                        ("x", fov),
                        ("y", fov),
                        ("mime-type", "download-fits"),
                        ("Sky-Survey", mode)])
        url = "%s?%s" % (DSS_URL, qs)
        # print("trying:", url)

        try:
            response = urlopen(url)
            # print("SUCCESS!")
            return response
        except HTTPError:
            pass
    else:
        print("NOT FOUND!")
        exit(1)


def chart(jpegfile, fitsfile, name, field_center, program):
    # open fits file without going to the disk
    ff = astropy.io.fits.open(fitsfile)

    # create the finding chart
    with closing(aplpy.FITSFigure(ff, auto_refresh=False)) as fig:
        fig.show_grayscale(invert='true', pmin=0.25, pmax=99.75)
        fig.tick_labels.set_font(size='small')

        # labels
        #fig.add_scalebar(0.0833333333333)
        #fig.scalebar.set_label('5\'')
        # fig.add_scalebar(0.1133333)
        # fig.scalebar.set_label('6.8\' (FORS)')


        if name:
            fig.add_label(0.2, 1.1, 'Object:', relative=True, weight='bold',
                          size='x-large', horizontalalignment='right')
            fig.add_label(0.22, 1.1,  name, relative=True, weight='normal',
                          size='x-large', horizontalalignment='left')

        fig.add_label(0.2, 1.02, 'Field Center:', relative=True, weight='bold',
                      size='small', horizontalalignment='right')
        fig.add_label(0.22, 1.02, field_center, relative=True, weight='normal',
                      size='small', horizontalalignment='left')

        if program:
            fig.add_label(1.0, -0.1, program, relative=True, weight='bold',
                          size='x-large', horizontalalignment='right')

        fig.add_label(0.0, -0.1, program_version, relative=True, weight='normal',
                      size='xx-small', horizontalalignment='left', color='grey')

        fig.save('zz.jpg')
        fig.save(jpegfile, format='jpeg')


def right_ascension(ra):
    """Check format of RA."""
    if fullmatch(r"([01][0-9]|2[0-3]):[0-5][0-9]:[0-5][0-9](\.\d*)?", ra):
        return ra
    raise ValueError("ra format must be HH:MM:SS")


def declination(dec):
    """Check format of RA."""
    if fullmatch(r"[-+]?([0-8][0-9]:[0-5][0-9]:[0-5][0-9](\.\d*)?|90:00:00)",
                 dec):
        return dec
    raise ValueError("declination format must be +DD:MM:SS")


def main():
    parser = argparse.ArgumentParser(description="""
    Stand-alone finding chart generation using images from DSS.
    """)
    parser.add_argument("name", help="target name")
    parser.add_argument("ra", help="format: hh:mm:ss", type=right_ascension)
    parser.add_argument("dec", help="format: +dd:mm:ss", type=declination)
    parser.add_argument("-p", "--program", dest="program", default="",
                        help="program ID")
    parser.add_argument("-f", "--fov", dest="fov", default=10., type=float,
                        help="field of view in arcmin")
    parser.add_argument("-v", "--version", action="version",
                        version="%(prog)s " + __version__)
    args = parser.parse_args()
    # print(args)

    with search_dss(args.ra, args.dec, args.fov) as fitsdata:
        center = "%s %s" % (args.ra, args.dec)
        chart('zz.jpg', fitsdata, args.name, center, args.program)
    exit(0)

if __name__ == '__main__':
    main()

# vim: set ai et ts=4 sw=4:
